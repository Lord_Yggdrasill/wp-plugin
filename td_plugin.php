<?php
/*
 * Plugin Name: TD Wordpress Plugin modélisme
 * Description: Plugin de gestion des clubs, membres et championnats
 * Author: Flo
 * Version: 1.0.0
 */

require_once plugin_dir_path(__FILE__ )."/Class_Wp_Club_Registration.php";
require_once plugin_dir_path(__FILE__ )."/Class_Wp_Liste_Club.php";
require_once plugin_dir_path(__FILE__ )."/Class_Wp_Member_Registration.php";
require_once plugin_dir_path(__FILE__ )."/Class_Wp_Liste_Members.php";
require_once plugin_dir_path(__FILE__ )."/Class_Wp_CompAuto_Registration.php";
require_once plugin_dir_path(__FILE__ )."/Class_Wp_Liste_CompAuto.php";
require_once plugin_dir_path(__FILE__ )."/Class_Wp_CompDrone_Registration.php";
require_once plugin_dir_path(__FILE__ )."/Class_Wp_Liste_CompDrone.php";

class registration_club
{
    public function __construct()
    {
        //Creation des tables lors de l'activation du plugin
        register_activation_hook(__FILE__, array('Class_Wp_Club_Registration', 'install'));

        //vidage des tables lors de la désactivation
        register_deactivation_hook(__FILE__, array('Class_Wp_Club_Registration', 'deactivate'));

        //suppression des tables lors de la suppression du plugin
        register_deactivation_hook(__FILE__, array('Class_Wp_Club_Registration', 'uninstall'));

//        add_action('widgets_init', function (){
//            register_widget('Class_Wp_Widget_Meteo');
//        });

//        add_action('wp_enqueue_scripts', array([$this, 'setting_link_script']));
        add_action('admin_menu', array($this, 'add_menu_back'));
    }

    public function add_menu_back()
    {
        $hook = add_menu_page("Clubs modélisme", "Liste des clubs de modélisme",
            "manage_options", "clubMod",
            array($this, 'myClubBack'), 'dashicon-admin-users', 46);
        add_submenu_page("clubMod", "Ajouter un club",
            'Ajouter', "manage_options", 'addClub', array($this, 'myClubBack') );
    }

    //fonction d'affichage de la page de données
    public function myClubBack()
    {
        echo "<h1>".get_admin_page_title()."</h1>";

        $ins = new Class_Wp_Club_Registration();

        if ($_REQUEST['page'] == 'clubMod' || isset($_POST['nom'])) { // liste des clubs
            if (isset($_POST['nom'])) {
                $ins->saveClub();
            }

            $table = new Class_Wp_Liste_Club();
            $table->prepare_items();
            echo $table->display();
        } else { //formulaire
            echo "<form method='post'> ".
                "<p>".
                "<label for='nom'>Nom</label>".
                "<input type='text' id='' name='nom' class='widefat' />".
                "</p>".
                "<p>".
                "<label for='rue'>Rue</label>".
                "<input type='text' id='' name='rue' class='widefat' />".
                "</p>".
                "<p>".
                "<label for='ville'>Ville</label>".
                "<input type='text' id='' name='ville' class='widefat' />".
                "<p>".
                "<label for='cp'>Code postal</label>".
                "<input type='text' id='' name='cp' class='widefat' />".
                "<p>".
                "<label for='region'>Région</label>".
                "<input type='text' id='' name='region' class='widefat' />".
                "<p>".
                "<label for='email'>Email</label>".
                "<input type='text' id='' name='email' class='widefat' />".
                "</p>".
                "<label for='tel'>Téléphone</label>".
                "<input type='text' id='' name='tel' class='widefat' />".
                "<p>".
                "<label for='categorie'>Catégorie</label>".
                "<input type='text' id='' name='categorie' class='widefat' />".
                "<p>".
                "<label for='nom_competition'>Nom de la compétition</label>".
                "<input type='text' id='' name='nom_competition' class='widefat' />".
                "<p><p><input type='submit' value='Enregistrer'/></p></form>";
        }


//       $ins = new Class_Wp_Club_Registration();
//
//       echo "<table class='widefat fixed' cellspacing='0'>".
//       "<tr><th class='manage-column column-columnname'>Nom</th>".
//       "<th class='manage-column column-columnname'>Rue</th>".
//       "<th class='manage-column column-columnname'>Ville</th>".
//       "<th class='manage-column column-columnname'>CP</th>".
//       "<th class='manage-column column-columnname'>Mail</th>".
//       "<th class='manage-column column-columnname'>Téléphone</th>".
//       "<th class='manage-column column-columnname'>Catégorie</th>".
//       "<th class='manage-column column-columnname'>Compétition</th></tr>";
//       foreach ($ins->findAll() as $line) {
//           echo "<tr>";
//           echo "<td>".$line['nom']."</td>";
//           echo "<td>".$line['rue']."</td>";
//           echo "<td>".$line['ville']."</td>";
//           echo "<td>".$line['CP']."</td>";
//           echo "<td>".$line['email']."</td>";
//           echo "<td>".$line['tel']."</td>";
//           echo "<td>".$line['categorie']."</td>";
//           echo "<td>".$line['nom_competition']."</td>";
//           echo "</tr>";
//       }
//       echo "</table>";
    }

//    public function setting_link_script()
//    {
//        wp_register_style('météo', plugins_url('meteo/meteo.css', __FILE__));
//        wp_enqueue_style('meteo');
//    }
}

class registration_member
{
    public function __construct()
    {
        //Creation des tables lors de l'activation du plugin
        register_activation_hook(__FILE__, array('Class_Wp_Member_Registration', 'install'));

        //vidage des tables lors de la désactivation
        register_deactivation_hook(__FILE__, array('Class_Wp_Member_Registration', 'deactivate'));

        //suppression des tables lors de la suppression du plugin
        register_deactivation_hook(__FILE__, array('Class_Wp_Member_Registration', 'uninstall'));

//        add_action('widgets_init', function (){
//            register_widget('Class_Wp_Widget_Meteo');
//        });

//        add_action('wp_enqueue_scripts', array([$this, 'setting_link_script']));
        add_action('admin_menu', array($this, 'add_menu_back2'));
    }

    public function add_menu_back2()
    {
        $hook = add_menu_page("Membres clubs", "Liste des membres du club",
            "manage_options", "clubMember",
            array($this, 'myClubMemberBack'), 'dashicon-admin-users', 46);
        add_submenu_page("clubMember", "Ajouter un membre",
            'Ajouter', "manage_options", 'addClubMember', array($this, 'myClubMemberBack') );
    }

    //fonction d'affichage de la page de données pour les membres du club
    public function myClubMemberBack()
    {
        echo "<h1>".get_admin_page_title()."</h1>";

        $ins = new Class_Wp_Member_Registration();

        if ($_REQUEST['page'] == 'clubMember' || isset($_POST['nom'])) { // liste des clubs
            if (isset($_POST['nom'])) {
                $ins->saveClubMember();
            }

            $table2 = new Class_Wp_Liste_Members();
            $table2->prepare_items2();
            echo $table2->display();
        } else { //formulaire
            echo "<form method='post'> ".
                "<p>".
                "<label for='nom'>Nom</label>".
                "<input type='text' id='' name='nom' class='widefat' />".
                "</p>".
                "<p>".
                "<label for='prenom'>Prenom</label>".
                "<input type='text' id='' name='prenom' class='widefat' />".
                "</p>".
                "<p>".
                "<label for='rue'>Rue</label>".
                "<input type='text' id='' name='rue' class='widefat' />".
                "</p>".
                "<p>".
                "<label for='ville'>Ville</label>".
                "<input type='text' id='' name='ville' class='widefat' />".
                "<p>".
                "<label for='cp'>Code postal</label>".
                "<input type='text' id='' name='cp' class='widefat' />".
                "<p>".
                "<label for='email'>Email</label>".
                "<input type='text' id='' name='email' class='widefat' />".
                "</p>".
                "<label for='tel'>Téléphone</label>".
                "<input type='text' id='' name='tel' class='widefat' />".
                "<p>".
                "<label for='num_adherent'>N° adhérent</label>".
                "<input type='text' id='' name='num_adherent' class='widefat' />".
                "<p>".
                "<label for='club'>Nom du club</label>".
                "<input type='text' id='' name='club' class='widefat' />".
                "<p><p><input type='submit' value='Enregistrer'/></p></form>";
        }
    }

//    public function setting_link_script()
//    {
//        wp_register_style('météo', plugins_url('meteo/meteo.css', __FILE__));
//        wp_enqueue_style('meteo');
//    }
}

class registration_comp1
{
    public function __construct()
    {
        //Creation des tables lors de l'activation du plugin
        register_activation_hook(__FILE__, array('Class_Wp_CompAuto_Registration', 'install'));

        //vidage des tables lors de la désactivation
        register_deactivation_hook(__FILE__, array('Class_Wp_CompAuto_Registration', 'deactivate'));

        //suppression des tables lors de la suppression du plugin
        register_deactivation_hook(__FILE__, array('Class_Wp_CompAuto_Registration', 'uninstall'));

//        add_action('widgets_init', function (){
//            register_widget('Class_Wp_Widget_Meteo');
//        });

//        add_action('wp_enqueue_scripts', array([$this, 'setting_link_script']));
        add_action('admin_menu', array($this, 'add_menu_back3'));
    }

    public function add_menu_back3()
    {
        $hook = add_menu_page("Classement championnat automobiles", "Classement comp automobiles",
            "manage_options", "comp1",
            array($this, 'myRankingBack'), 'dashicon-admin-users', 46);
        add_submenu_page("comp1", "Ajouter/modifier un participant",
            'Ajouter', "manage_options", 'manageComp1', array($this, 'myRankingBack') );
        add_submenu_page("comp1Modif", "Modifier les participants",
            'Modifier', "manage_options", 'modifComp1', array($this, 'myRankingModif') );
    }

    //fonction d'affichage de la page de données pour les membres du club
    public function myRankingBack()
    {
        echo "<h1>".get_admin_page_title()."</h1>";

        $ins = new Class_Wp_CompAuto_Registration();

        if ($_REQUEST['page'] == 'comp1' || isset($_POST['nom'])) { // liste classement
            if (isset($_POST['nom'])) {
                $ins->saveRanking();
            }

            $table2 = new Class_Wp_Liste_CompAuto();
            $table2->prepare_items2();
            echo $table2->display();
        } else { //formulaire
            echo "<form method='post'> ".
                "<p>".
                "<label for='position'>Position</label>".
                "<input type='text' id='' name='position' class='widefat' />".
                "</p>".
                "<p>".
                "<label for='points'>Points</label>".
                "<input type='text' id='' name='points' class='widefat' />".
                "</p>".
                "<p>".
                "<label for='nom'>Nom</label>".
                "<input type='text' id='' name='nom' class='widefat' />".
                "</p>".
                "<p>".
                "<label for='prenom'>Prénom</label>".
                "<input type='text' id='' name='prenom' class='widefat' />".
                "</p>".
                "<label for='club'>Nom du club</label>".
                "<input type='text' id='' name='club' class='widefat' />".
                "</p>".
                "<p>".
                "<label for='nb_courses'>Courses</label>".
                "<input type='text' id='' name='nb_courses' class='widefat' />".
                "<p><p><input type='submit' value='Enregistrer'/></p></form>";
        }
    }
}

class registration_comp2
{
    public function __construct()
    {
        //Creation des tables lors de l'activation du plugin
        register_activation_hook(__FILE__, array('Class_Wp_CompDrone_Registration', 'install'));

        //vidage des tables lors de la désactivation
        register_deactivation_hook(__FILE__, array('Class_Wp_CompDrone_Registration', 'deactivate'));

        //suppression des tables lors de la suppression du plugin
        register_deactivation_hook(__FILE__, array('Class_Wp_CompDrone_Registration', 'uninstall'));

//        add_action('widgets_init', function (){
//            register_widget('Class_Wp_Widget_Meteo');
//        });

//        add_action('wp_enqueue_scripts', array([$this, 'setting_link_script']));
        add_action('admin_menu', array($this, 'add_menu_back3'));
    }

    public function add_menu_back3()
    {
        $hook = add_menu_page("Classement championnat drone", "Classement comp drones",
            "manage_options", "comp2",
            array($this, 'myRankingBack'), 'dashicon-admin-users', 46);
        add_submenu_page("comp2", "Ajouter/modifier un participant",
            'Ajouter', "manage_options", 'manageComp2', array($this, 'myRankingBack'));
        add_submenu_page("comp2Modif", "Modifier les participants",
            'Modifier', "manage_options", 'modifComp2', array($this, 'myRankingModif'));
    }

    //fonction d'affichage de la page de données pour les membres du club
    public function myRankingBack()
    {
        echo "<h1>" . get_admin_page_title() . "</h1>";

        $ins = new Class_Wp_CompDrone_Registration();

        if ($_REQUEST['page'] == 'comp2' || isset($_POST['nom'])) { // liste classement
            if (isset($_POST['nom'])) {
                $ins->saveRanking();
            }

            $table2 = new Class_Wp_Liste_CompDrone();
            $table2->prepare_items2();
            echo $table2->display();
        } else { //formulaire
            echo "<form method='post'> " .
                "<p>" .
                "<label for='position'>Position</label>" .
                "<input type='text' id='' name='position' class='widefat' />" .
                "</p>" .
                "<p>" .
                "<label for='points'>Points</label>" .
                "<input type='text' id='' name='points' class='widefat' />" .
                "</p>" .
                "<p>" .
                "<label for='nom'>Nom</label>" .
                "<input type='text' id='' name='nom' class='widefat' />" .
                "</p>" .
                "<p>" .
                "<label for='prenom'>Prénom</label>" .
                "<input type='text' id='' name='prenom' class='widefat' />" .
                "</p>" .
                "<label for='club'>Nom du club</label>" .
                "<input type='text' id='' name='club' class='widefat' />" .
                "</p>" .
                "<label for='rotors'>Rotors</label>" .
                "<input type='text' id='' name='rotors' class='widefat' />" .
                "</p>" .
                "<label for='manches'>Manches</label>" .
                "<input type='text' id='' name='manches' class='widefat' />" .
                "</p>" .
                "<p>" .
                "<label for='nb_courses'>Courses</label>" .
                "<input type='text' id='' name='nb_courses' class='widefat' />" .
                "<p><p><input type='submit' value='Enregistrer'/></p></form>";
        }
    }
}

new registration_club();
new registration_member();
new registration_comp1();
new registration_comp2();