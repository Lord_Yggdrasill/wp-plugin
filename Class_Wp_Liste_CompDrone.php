<?php

if (! class_exists('WP_List_Table')) {
    require_once (ABSPATH.'wp-admin/includes/class-wp-list-table.php');
}

require_once plugin_dir_path(__FILE__).'/Class_Wp_CompDrone_Registration.php';

class Class_Wp_Liste_CompDrone extends WP_List_Table
{
    private $dal;

    public function __construct($args = array())
    {
        parent::__construct([
            'singular' => __('Club', 'sp'),
            'plural' => __('Clubs', 'sp')
        ]);
        $this->dal = new Class_Wp_CompDrone_Registration();
    }

    /**
     * preparation de la table
     */

    public function prepare_items2()
    {
        $columns = $this->get_columns();
        $hidden = $this->get_hidden_columns();
        $sortable = $this->get_sortable_columns();

        $this->process_bulk_action();

        $perPage = $this->get_items_per_page('club_per_page', 5);
        $currentPage = $this->get_pagenum();
        $data = $this->dal->findRanking();
        $totalPage = count($data);

        usort($data, array(&$this, 'usort_reorder'));
        $paginateData = array_slice($data, (($currentPage -1) * $perPage), $perPage);
        $this->set_pagination_args([
            'total_item' => $totalPage,
            'per_page' => $perPage
        ]);

        $this->_column_headers = array($columns, $hidden, $sortable);
        $this->items = $paginateData;
    }

    //retourne des colonnes
    public function get_columns()
    {
        $columns = [
            'cb' => '<input type ="checkbox">',
            'position' => 'position',
            'points' => 'points',
            'nom' => 'nom',
            'prenom' => 'prenom',
            'club' => 'club',
            'rotors' => 'rotors',
            'manches' => 'manches',
            'nb_courses' => 'nb_courses',
        ];
        return $columns;
    }


    /**
     * colonne cachée
     */
    public function get_hidden_columns()
    {
        return array();
    }

    /**
     * gestion des colonnes triables
     */
    public function get_sortable_columns()
    {
        return $sortable = array(
            'nom' => array('nom', true),
            'prenom' => array('prenom', true),
            'position' => array('position', true),
        );
    }

    public function column_default($item, $column_name)
    {
        switch ($column_name){
            case 'id':
            case 'position':
            case 'points':
            case 'nom':
            case 'prenom':
            case 'club':
            case 'rotors':
            case 'manches':
            case 'nb_courses':
                return $item[$column_name];
            default:
                return print_r( $item, true );
        }
    }

    public function usort_reorder($a, $b)
    {
        $orderby = ( !empty($_GET['orderby']) ) ? $_GET['orderby'] : 'nom';
        $order = ( !empty($_GET['order']) ) ? $_GET['order'] : 'asc';
        $result = strcmp($a[$orderby], $b[$orderby]);
        return ( $order === 'asc') ? $result : -$result;
    }


    /**
     * Ajout des sélecteurs d'actions
     * @return array|string[]
     */
    function get_bulk_actions()
    {
        $actions = [
            'delete' => 'Supprimer',
            'voir' => 'Voir'
        ];
        return $actions;
    }

    //Défintion des checkbox de chaque lignes
    function column_cb($item)
    {
        return sprintf('<input type="checkbox" name="id[]" value="%s">', $item['id']);
    }

    //ecouteur filtre action
    function process_bulk_action()
    {
        if ('delete' === $this->current_action()) {
            $ids = isset($_REQUEST['id']) ? $_REQUEST['id'] : array();
            if (!empty($ids)) {
                $this->dal->deleteById($ids);
            }
        }
    }

    /**
     * Action directe sur le nom
     */
    function column_nom($item)
    {
        $actions = [
            'delete' => sprintf("<a href='?page=%s&action=%s&id=%s'>Supprimer</a>", $_REQUEST['page'],
                'delete', $item['id']),
            'voir' => sprintf("<a href='?page=%s&action=%s&id=%s'>Voir</a>", $_REQUEST['page'],
                'voir', $item['id']),
        ];
        return sprintf('%1$s %2$s', $item['nom'], $this->row_actions($actions));
    }

}