<?php


class Class_Wp_Ern_Registration
{
    public function __construct()
    {

    }

    public static function install()
    {
        global $wpdb;
        $wpdb->query("CREATE TABLE IF NOT EXISTS ".
        "{$wpdb->prefix}ern_member(id INT AUTO_INCREMENT PRIMARY KEY,".
        "nom VARCHAR (150) NOT NULL, prenom VARCHAR(255) NOT NULL, age INT NOT NULL,".
        "email VARCHAR(255) NOT NULL);");

        $count = $wpdb->get_var("SELECT count(*) FROM {$wpdb->prefix}ern_member;");

        if ($count == 0) {
            $wpdb->insert("{$wpdb->prefix}ern_member", array(
                'nom' => 'Gonzalez',
                'prenom' => 'Florent',
                'age' => 30,
                'email' => 'florent_gonzalez@hotmail.com'
            ));
        }
    }

    //fonction lors de la désactivation
    public static function deactivate()
    {
        global $wpdb;
        $wpdb->query("TRUNCATE {$wpdb->prefix}ern_member;");
    }

    //fonction lors de la suppression du plugin
    public static function uninstall()
    {
        global $wpdb;
        $wpdb->query("DROP TABLE IF EXISTS {$wpdb->prefix}ern_member;");
    }

    /**
     * Lister tous les membres
     * @return array|object|null
     */
    public function findAll(){
        global $wpdb;
        $res = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}ern_member;", ARRAY_A);
        return $res;
    }

    public function saveMember()
    {
        global $wpdb;
        if (isset($_POST['email']) && !empty($_POST['email'])) {
            $nom = $_POST['nom'];
            $prenom = $_POST['prenom'];
            $age = (is_numeric($_POST['age']))?($_POST['age']): 0;
            $email = $_POST['email'];

            $row = $wpdb->get_row(("SELECT * FROM {$wpdb->prefix}ern_member WHERE email = '{$email}';"));

            if (is_null($row)) {
                $wpdb->insert("{$wpdb->prefix}ern_member", array(
                   'nom' => $nom,
                   'prenom' => $prenom,
                   'age' => $age,
                   'email' => $email,
                ));
            }
        }
    }

    public function deleteById($ids)
    {
        if (!is_array($ids)) { //si ids n'est pas un tableau
            $ids = array($ids); //on créé le tableau à une valeur
        }

        global $wpdb;

        $wpdb->query("DELETE FROM {$wpdb->prefix}ern_member WHERE id IN(".implode(',', $ids).");");
    }

}