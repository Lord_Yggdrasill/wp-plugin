<?php


class Class_Wp_CompAuto_Registration
{

    public function __construct()
    {

    }

    public static function install()
    {
        global $wpdb;
        $wpdb->query("CREATE TABLE IF NOT EXISTS ".
            "{$wpdb->prefix}competition_auto(id INT AUTO_INCREMENT PRIMARY KEY,".
            "`position` INT NOT NULL, points INT NOT NULL, nom VARCHAR(255) NOT NULL, prenom VARCHAR(255) NOT NULL,".
            " club VARCHAR(255) NOT NULL, nb_courses INT NOT NULL);");

        $count = $wpdb->get_var("SELECT count(*) FROM {$wpdb->prefix}competition_auto;");

        if ($count == 0) {
            $wpdb->insert("{$wpdb->prefix}competition_auto", array(
                'position' => 1,
                'points' => 0,
                'nom' => 'Gonzalez',
                'prenom' => 'Florent',
                'club' => 'Club test',
                'nb_courses' => 0,
            ));
        }
    }

    //fonction lors de la désactivation
    public static function deactivate()
    {
        global $wpdb;
        $wpdb->query("TRUNCATE {$wpdb->prefix}competition_auto;");
    }

    /**
     * Lister tous les clubs
     * @return array|object|null
     */
    public function findRanking(){
        global $wpdb;
        $res = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}competition_auto;", ARRAY_A);
        return $res;
    }

    public function saveRanking()
    {
        global $wpdb;
        //si un nom est posté, je passe tous les champs postés dans des variables
        if (isset($_POST['nom']) && !empty($_POST['nom'])) {
            $position = $_POST['position'];
            $points = $_POST['points'];
            $nom = $_POST['nom'];
            $prenom = $_POST['prenom'];
            $club = $_POST['club'];
            $nb_courses = $_POST['nb_courses'];

            //Je sélectionne dans la BDD la ligne où le nom correspond au nom posté
            $row = $wpdb->get_row(("SELECT * FROM {$wpdb->prefix}competition_auto WHERE nom = '{$nom}';"));

            //si la ligne est nulle (le nom n'a pas été trouvé dans la BDD)
            //j'insere dans la BDD les valeurs postées via le formuaire
            if (is_null($row)) {
                $wpdb->insert("{$wpdb->prefix}competition_auto", array(
                    'position' => $position,
                    'points' => $points,
                    'nom' => $nom,
                    'prenom' => $prenom,
                    'club' => $club,
                    'nb_courses' => $nb_courses,
                ));
                //si la ligne concernant le nom a été trouvée, je fais un update de la ligne avec les nouvelles
                //infos qui son postées dans le formulaire
            } elseif ($row) {
                $wpdb->update("{$wpdb->prefix}competition_auto", array(
                    'position' => $position,
                    'points' => $points,
                    'nom' => $nom,
                    'prenom' => $prenom,
                    'club' => $club,
                    'nb_courses' => $nb_courses,
                ), array(
                    'nom' => $nom,
                ));
            }
        }
    }

//    public function modifRanking()
//    {
//        global $wpdb;
//        if (isset($_POST['nom']) && !empty($_POST['nom'])) {
//            $position = $_POST['position'];
//            $points = $_POST['points'];
//            $nom = $_POST['nom'];
//            $prenom = $_POST['prenom'];
//            $club = $_POST['club'];
//
//            $row = $wpdb->get_row(("SELECT * FROM {$wpdb->prefix}competition_auto WHERE nom = '{$nom}';"));
//
//            $modify_ranking = $wpdb->prepare("UPDATE {$wpdb->prefix}competition_auto SET `position` = :rank, `points` = :points,
//             `nom` = :nom, `prenom` = :prenom, `club` = :club");
//            $modify_ranking->execute(array(
//                'rank' => $position,
//                'points' => $points,
//                'nom' => $nom,
//                'prenom' => $prenom,
//                'club' => $club,
//            ));
////            if (is_null($row)) {
////                $wpdb->insert("{$wpdb->prefix}competition_auto", array(
////                    'position' => $position,
////                    'points' => $points,
////                    'nom' => $nom,
////                    'prenom' => $prenom,
////                    'club' => $club,
////                ));
////            }
//        }
//    }

    public function deleteById($ids)
    {
        if (!is_array($ids)) { //si ids n'est pas un tableau
            $ids = array($ids); //on créé le tableau à une valeur
        }

        global $wpdb;

        $wpdb->query("DELETE FROM {$wpdb->prefix}competition_auto WHERE id IN(".implode(',', $ids).");");
    }
}