<?php
/*
 * Plugin Name: ERN plugin
 * Description: Plugin de l'ern
 * Author: Nous
 * Version: 1.0.0
 */

require_once plugin_dir_path(__FILE__ )."/Class_Wp_Widget_Meteo.php";
require_once plugin_dir_path(__FILE__ )."/Class_Wp_Ern_Registration.php";
require_once plugin_dir_path(__FILE__ )."/Class_Wp_Liste_Ern.php";

class registration
{
   public function __construct()
   {
       //Creation des tables lors de l'activation du plugin
       register_activation_hook(__FILE__, array('Class_Wp_Ern_Registration', 'install'));

       //vidage des tables lors de la désactivation
       register_deactivation_hook(__FILE__, array('Class_Wp_Ern_Registration', 'deactivate'));

       //suppression des tables lors de la suppression du plugin
       register_deactivation_hook(__FILE__, array('Class_Wp_Ern_Registration', 'uninstall'));

        add_action('widgets_init', function (){
            register_widget('Class_Wp_Widget_Meteo');
        });

//        add_action('wp_enqueue_scripts', array([$this, 'setting_link_script']));
        add_action('admin_menu', array($this, 'add_menu_back'));
   }

   public function add_menu_back()
   {
        $hook = add_menu_page("Les membres de l'ern", "Membre Ern",
            "manage_options", "ernMember",
            array($this, 'myMemberBack'), 'dashicon-admin-users', 46);
        add_submenu_page("ernMember", "Ajouter un membre",
        'Ajouter', "manage_options", 'addMember', array($this, 'myMemberBack') );
   }

   //fonction d'affichage de la page de données
   public function myMemberBack()
   {
       echo "<h1>".get_admin_page_title()."</h1>";

       $ins = new Class_Wp_Ern_Registration();

       if ($_REQUEST['page'] == 'ernMember' || isset($_POST['nom'])) { // liste des membres
           if (isset($_POST['nom'])) {
               $ins->saveMember();
           }

           $table = new Class_Wp_Liste_Ern();
           $table->prepare_items();
           echo $table->display();
       } else { //formulaire
             echo "<form method='post'> ".
               "<p>".
               "<label for='nom'>Nom</label>".
               "<input type='text' id='' name='nom' class='widefat' />".
               "</p>".
               "<p>".
               "<label for='prenom'>Prenom</label>".
               "<input type='text' id='' name='prenom' class='widefat' />".
               "</p>".
               "<p>".
               "<label for='age'>Age</label>".
               "<input type='text' id='' name='age' class='widefat' />".
               "<p>".
               "<label for='email'>Email</label>".
               "<input type='text' id='' name='email' class='widefat' />".
               "</p><p><input type='submit' value='Enregistrer'/></p></form>";
       }


//       $ins = new Class_Wp_Ern_Registration();
//
//       echo "<table class='widefat fixed' cellspacing='0'>".
//       "<tr><th class='manage-column column-columnname'>Nom</th>".
//       "<th class='manage-column column-columnname'>Prénom</th>".
//       "<th class='manage-column column-columnname'>Age</th>".
//       "<th class='manage-column column-columnname'>Mail</th></tr>";
//       foreach ($ins->findAll() as $line) {
//           echo "<tr>";
//           echo "<td>".$line['nom']."</td>";
//           echo "<td>".$line['prenom']."</td>";
//           echo "<td>".$line['age']."</td>";
//           echo "<td>".$line['email']."</td>";
//           echo "</tr>";
//       }
//       echo "</table>";
   }

   public function setting_link_script()
   {
       wp_register_style('météo', plugins_url('meteo/meteo.css', __FILE__));
       wp_enqueue_style('meteo');
   }
}

new registration();