<?php


class Class_Wp_Club_Registration
{

    public function __construct()
    {

    }

    public static function install()
    {
        global $wpdb;
        $wpdb->query("CREATE TABLE IF NOT EXISTS ".
            "{$wpdb->prefix}clubs(id INT AUTO_INCREMENT PRIMARY KEY,".
            "nom VARCHAR (150) NOT NULL, rue VARCHAR(255) NOT NULL, ville VARCHAR(150) NOT NULL, cp INT NOT NULL, region VARCHAR(255) NOT NULL".
            "email VARCHAR(255) NOT NULL, tel VARCHAR(10) NOT NULL, categorie VARCHAR(255) NOT NULL, nom_competition VARCHAR(255) NOT NULL);");

        $count = $wpdb->get_var("SELECT count(*) FROM {$wpdb->prefix}clubs;");

        if ($count == 0) {
            $wpdb->insert("{$wpdb->prefix}clubs", array(
                'nom' => 'Club test',
                'rue' => '1 rue Léon Blum',
                'ville' => 'Le Soler',
                'cp' => 66270,
                'region' => 'Occitanie',
                'email' => 'club.test@gmail.com',
                'tel' => '0650515253',
                'categorie' => 'naval',
                'nom_competition' => 'competition_2020',
            ));
        }
    }

    //fonction lors de la désactivation
    public static function deactivate()
    {
        global $wpdb;
        $wpdb->query("TRUNCATE {$wpdb->prefix}clubs;");
    }

    /**
     * Lister tous les clubs
     * @return array|object|null
     */
    public function findAll(){
        global $wpdb;
        $res = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}clubs;", ARRAY_A);
        return $res;
    }

    public function saveClub()
    {
        global $wpdb;
        if (isset($_POST['nom']) && !empty($_POST['nom'])) {
            $nom = $_POST['nom'];
            $rue = $_POST['rue'];
            $ville = $_POST['ville'];
            $cp = $_POST['cp'];
            $region = $_POST['region'];
            $email = $_POST['email'];
            $tel = $_POST['tel'];
            $categorie = $_POST['categorie'];
            $nom_competition = $_POST['nom_competition'];

            $row = $wpdb->get_row(("SELECT * FROM {$wpdb->prefix}clubs WHERE nom = '{$nom}';"));

            if (is_null($row)) {
                $wpdb->insert("{$wpdb->prefix}clubs", array(
                    'nom' => $nom,
                    'rue' => $rue,
                    'ville' => $ville,
                    'cp' => $cp,
                    'region' => $region,
                    'email' => $email,
                    'tel' => $tel,
                    'categorie' => $categorie,
                    'nom_competition' => $nom_competition,
                ));
            }
        }
    }
    public function deleteById($ids)
    {
        if (!is_array($ids)) { //si ids n'est pas un tableau
            $ids = array($ids); //on créé le tableau à une valeur
        }

        global $wpdb;

        $wpdb->query("DELETE FROM {$wpdb->prefix}clubs WHERE id IN(".implode(',', $ids).");");
    }
}