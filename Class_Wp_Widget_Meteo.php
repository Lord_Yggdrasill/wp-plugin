<?php


class Class_Wp_Widget_Meteo extends WP_Widget
{
    public function __construct()
    {
        $widget_ops = array(
            'className' => 'ern_meteo',
            'description' => __("Informations de Meteo pour le WP de l'ern"),
            'customize_selected_refresh' => true
        );
        parent::__construct('wheather', __('Meteo Widget', 'Meteo'), $widget_ops);
    }

    // contruction du formulaire de backend
    public function form($instance)
    {
        $instance = wp_parse_args( (array) $instance,
            array(
                'lat' => '',
                'lon' => '',
                'api' => ''
            )
        );
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('lat') ?>">Latitude</label>
            <input type="text"
                   class="widefat"
                   id="<?php echo $this->get_field_id('lat') ?>"
                   name="<?php echo $this->get_field_name('lat') ?>"
                   value="<?php echo esc_attr( $instance['lat'] ) ?>">
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('lon') ?>">Longitude</label>
            <input type="text"
                   class="widefat"
                   id="<?php echo $this->get_field_id('lon') ?>"
                   name="<?php echo $this->get_field_name('lon') ?>"
                   value="<?php echo esc_attr( $instance['lon'] ) ?>">
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('apitoken') ?>">Clé Api</label>
            <input type="text"
                   class="widefat"
                   id="<?php echo $this->get_field_id('apitoken') ?>"
                   name="<?php echo $this->get_field_name('apitoken') ?>"
                   value="<?php echo esc_attr( $instance['apitoken'] ) ?>">
        </p>
        <?php
    }

    //mise à jour des données
    public function update($new_instance, $old_instance)
    {
        $instance = $old_instance;
        $instance['lat'] = sanitize_text_field( $new_instance['lat'] );
        $instance['lon'] = sanitize_text_field( $new_instance['lon'] );
        $instance['apitoken'] = sanitize_text_field( $new_instance['apitoken'] );

        return $instance;
    }

    /**
     * affichage frontend
     * @param array $args
     * @param array $instance
     */
    public function widget($args, $instance)
    {
        $title = "Météo";
        $url = 'https://api.darksky.net/forecast' .
            $instance['apitoken'] . '/' . $instance['lat'].','.$instance['lon'];
        $request = wp_remote_get($url);
        if ( is_wp_error( $request )) {
            return false;
        }

        $body = wp_remote_retrieve_body( $request );
        $data = json_decode( $body );

        echo $args['before_widget'];
        if ($title) {
            echo $args['before_title'] . $title . $args['after_title'];
        }
        echo '<div class="meteo_wrap" id="meteo_wrap">';
        if (!empty($data)) {
            echo '<div class="'.$data->currently->icon.'icon">&nbsp;</div>';
            echo '<div>'.number_format((($data->currently->temperature -32) * (5/9)),
                    2, ',', ' ').' deg</div>';
            echo '<div>'.number_format(($data->currently->windSpeed  * 1.609),
                    2, ',', ' ').' km</div>';
        }
        echo '</div>';
        echo $args['after_widget'];

        return '';
    }
}