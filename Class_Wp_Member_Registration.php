<?php


class Class_Wp_Member_Registration
{

    public function __construct()
    {

    }

    public static function install()
    {
        global $wpdb;
        $wpdb->query("CREATE TABLE IF NOT EXISTS ".
            "{$wpdb->prefix}club_member(id INT AUTO_INCREMENT PRIMARY KEY,".
            "nom VARCHAR (150) NOT NULL, prenom VARCHAR(255) NOT NULL, rue VARCHAR(255) NOT NULL, ville VARCHAR(150) NOT NULL, cp INT NOT NULL,".
            "email VARCHAR(255) NOT NULL, tel VARCHAR(10) NOT NULL, num_adherent INT NOT NULL, club VARCHAR(255) NOT NULL);");

        $count = $wpdb->get_var("SELECT count(*) FROM {$wpdb->prefix}club_member;");

        if ($count == 0) {
            $wpdb->insert("{$wpdb->prefix}club_member", array(
                'nom' => 'Dupont',
                'prenom' => 'Jean',
                'rue' => '2 rue de la sardane',
                'ville' => 'Saint-Cyprien',
                'cp' => 66750,
                'email' => 'dupont.jean@gmail.com',
                'tel' => '0606060606',
                'num_adherent' => 1,
                'club' => 'Club test',
            ));
        }
    }

    //fonction lors de la désactivation
    public static function deactivate()
    {
        global $wpdb;
        $wpdb->query("TRUNCATE {$wpdb->prefix}club_member;");
    }

    /**
     * Lister tous les clubs
     * @return array|object|null
     */
    public function findAllMembers(){
        global $wpdb;
        $res = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}club_member;", ARRAY_A);
        return $res;
    }

    public function saveClubMember()
    {
        global $wpdb;
        if (isset($_POST['nom']) && !empty($_POST['nom'])) {
            $nom = $_POST['nom'];
            $prenom = $_POST['prenom'];
            $rue = $_POST['rue'];
            $ville = $_POST['ville'];
            $cp = $_POST['cp'];
            $email = $_POST['email'];
            $tel = $_POST['tel'];
            $num_adherent = $_POST['num_adherent'];
            $club = $_POST['club'];

            $row = $wpdb->get_row(("SELECT * FROM {$wpdb->prefix}club_member WHERE nom = '{$nom}';"));
            $club_tied = $wpdb->get_row("SELECT nom FROM {$wpdb->prefix}clubs WHERE nom = '{$club}'");

            if (is_null($row) && $club_tied) {
                $wpdb->insert("{$wpdb->prefix}club_member", array(
                    'nom' => $nom,
                    'prenom' => $prenom,
                    'rue' => $rue,
                    'ville' => $ville,
                    'cp' => $cp,
                    'email' => $email,
                    'tel' => $tel,
                    'num_adherent' => $num_adherent,
                    'club' => $club,
                ));
            }
        }
    }
    public function deleteById($ids)
    {
        if (!is_array($ids)) { //si ids n'est pas un tableau
            $ids = array($ids); //on créé le tableau à une valeur
        }

        global $wpdb;

        $wpdb->query("DELETE FROM {$wpdb->prefix}club_member WHERE id IN(".implode(',', $ids).");");
    }
}